package ABC.api.model;

import java.util.Comparator;

public class Feed {

	Posts post;
	int buzzwords;
	

	public Feed(Posts post, int buzzwords) {
		super();
		this.post = post;
		this.buzzwords = buzzwords;
	}
	
	
	public Posts getPost() {
		return post;
	}
	public void setPost(Posts post) {
		this.post = post;
	}
	public int getBuzzwords() {
		return buzzwords;
	}
	public void setBuzzwords(int buzzwords) {
		this.buzzwords = buzzwords;
	}
	
	
}


