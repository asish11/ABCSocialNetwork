package ABC.api.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="follower")
public class Follower {

@Id
@GeneratedValue(strategy=GenerationType.IDENTITY)
@Column(name="id")
int id;

@Column(name="user_id")	
 int id1;

@Column(name="follower_id")	
 int id2;


public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public int getId1() {
	return id1;
}
public void setId1(int id1) {
	this.id1 = id1;
}
public int getId2() {
	return id2;
}
public void setId2(int id2) {
	this.id2 = id2;
}
 
 
	
}
