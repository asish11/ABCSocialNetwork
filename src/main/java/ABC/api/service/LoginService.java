package ABC.api.service;

import ABC.api.dao.LoginDao;
import ABC.api.model.Login;
import ABC.api.model.User;

public class LoginService {

	public User doLoginService(Login obj) {
		
		User loginResp = null;
		LoginDao dao = new LoginDao();
		loginResp=dao.doLoginDao(obj);
		
		return loginResp;
		
	}
	
}
