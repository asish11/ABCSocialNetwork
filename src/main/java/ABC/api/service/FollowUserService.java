package ABC.api.service;

import ABC.api.dao.FollowUserDao;
import ABC.api.model.Follower;

public class FollowUserService {

	public String doFollowUserService(Follower obj){
		
		FollowUserDao dao = new FollowUserDao();
		String status=dao.doFollowUserDao(obj);
		
		return status;
		
	}
	
}
