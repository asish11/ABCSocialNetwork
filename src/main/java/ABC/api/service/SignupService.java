package ABC.api.service;

import ABC.api.dao.SignupDao;
import ABC.api.model.*;

public class SignupService {

	public String doSignupService(Signup obj){
		
		SignupDao dao= new SignupDao();
		String status = dao.doSignupDao(obj);
		
		return status;
		
	}
	
	
}
