package ABC.api.service;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ABC.api.dao.PostsDao;
import ABC.api.model.Feed;
import ABC.api.model.Posts;

public class PostsService {
		
	public List<Feed> convertToFeedType(List<Posts> posts){
		
		List<Feed> feed = searchBuzzword(posts);
		List<Feed> sortedFeed = sortFeed(feed);
				
		return sortedFeed;
		
	}
		
	
	private List<Feed> sortFeed(List<Feed> feed) {
		
		Collections.sort(feed, new buzzWordComp());		
		return feed;
	}


	private List<Feed> searchBuzzword(List<Posts> posts) {
		
		Properties prop = new Properties();
		InputStream input = null;
		try {
			input= getClass().getClassLoader().getResourceAsStream("buzzwords.properties");
			prop.load(input);
			
			System.out.println("Property :::"+prop.getProperty("buzzwords"));
			
		} catch (Exception e) {
			System.out.println("Error @ Post Service - Possible File Load error");
			e.printStackTrace();
		}
		
		
		int buzzCount=0,buzzword=0;
		List<Feed> feed = new ArrayList<Feed>();
		String buzzwords[] = prop.getProperty("buzzwords").split(",");
				
		 for(Posts p:posts){
			 
			 buzzCount=0;		 
			 
			 for(String b : buzzwords){
				 
				 if(b.trim().length()>0){ // handle empty ',' delimiters
							 
					 Pattern pattern = Pattern.compile(b, Pattern.CASE_INSENSITIVE);
					 Matcher matches = pattern.matcher(p.getPostContent());
				 
					 while(matches.find()){					 
						 buzzword++;					 
					 }
				 }
							 
			 }
			 
			 if(buzzword>0){
				 buzzCount=buzzword;
				 buzzword=0;
			 }
			 
			 feed.add(new Feed(p,buzzCount));
			 			 
		}		
		
		return feed;
	}


	public List<Feed> getFeedService(int id){
		
		PostsDao dao= new PostsDao();
		List<Posts> posts=dao.getFeedDao(id);
		
		List<Feed> feed = convertToFeedType(posts);
		
		return feed;
		
	}
	
	public String makePostsService(Posts obj){
		
		PostsDao dao= new PostsDao();
		String status=dao.writePostDao(obj);
		
		return status;
	}

}

class buzzWordComp implements Comparator<Feed>{
	
	@Override
	public int compare(Feed f1,Feed f2){
		
		if(f1.getBuzzwords()<f2.getBuzzwords()){
			return 1;
		}
		else{
			return -1;
		}
		
	}
		
}
