package ABC.api.controller;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import ABC.api.model.Posts;
import ABC.api.service.PostsService;

@Path("posts")
public class PostsController {
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)	
	public Response writePost(Posts obj){
		
		PostsService service = new PostsService();
		String status = service.makePostsService(obj);
		
		if(status.equals("success")){
			System.out.println("Post has been successfully written in feed");
			return Response.status(201).entity(status).build();
		}
		else{
			System.out.println("Post could not be written in feed");
			return Response.status(500).entity(status).build();
		}
		
	}
	
}
