package ABC.api.controller;

import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

import ABC.api.model.Follower;
import ABC.api.service.FollowUserService;

@Path("{id1}/follows/{id2}")
public class FollowUserController {
	
@PUT
public Response followUser(@PathParam("id1") int id1,@PathParam("id2") int id2) {
	
	 Follower obj = new Follower();
	 obj.setId1(id1);
	 obj.setId2(id2);
	 FollowUserService service = new FollowUserService();
	 String status=service.doFollowUserService(obj);
	 if(status.equals("success")){
		 System.out.println("SUCCESS##User"+id1+"is following"+id2);
		 return Response.status(200).entity(status).build(); 
		 
	 }
	 else{
		 System.out.println("FAIL##User"+id1+"is not following"+id2);
		 return Response.status(500).entity(status).build();
	 }
	
	
}
	
	
}
