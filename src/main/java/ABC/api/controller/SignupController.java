package ABC.api.controller;

import javax.print.attribute.standard.Media;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import ABC.api.model.Signup;
import ABC.api.model.User;
import ABC.api.service.SignupService;

@Path("signup")
public class SignupController {

@POST
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public Response doSignup(Signup obj){
	
	String status = null;
	Response resp=null;
	
	SignupService service = new SignupService();
	status = service.doSignupService(obj);
	
	if(status.equals("success")) {		
		return Response.status(201).entity(status).build();
	}
	else {
		return Response.status(401).entity(status).build();
	}

	}

	
}
