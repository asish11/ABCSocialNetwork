package ABC.api.controller;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Path("health")
public class HealthCheck {

@GET
public String isUp() {
	
	return "Service is up and running";
	
}	
}
