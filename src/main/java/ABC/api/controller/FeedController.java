package ABC.api.controller;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import ABC.api.model.Feed;
import ABC.api.model.Posts;
import ABC.api.service.PostsService;

@Path("{id}/feed")
public class FeedController {

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getFeed(@PathParam("id") int id){
		
		PostsService service = new PostsService();
		List<Feed> feed = service.getFeedService(id);
		
		return Response.status(200).entity(feed).build();
	}
}
