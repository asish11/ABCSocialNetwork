package ABC.api.controller;

import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import ABC.api.model.Login;
import ABC.api.model.User;
import ABC.api.service.LoginService;

import javax.ws.rs.POST;

@Path("login")
public class LoginController {

@POST
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public Response doLogin(Login obj) {
	
	User loginResp=null;
	Response resp=null;
	LoginService service = new LoginService();
	loginResp=service.doLoginService(obj);

	if(loginResp!=null) {
		System.out.println("Login Success.");		
		return Response.status(200).entity("Login Success.").build();
	}
	else {
		System.out.println("Login Fail.");
		return Response.status(401).entity("Login Fail.").build();
	}

	}
	
}

