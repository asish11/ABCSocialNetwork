package ABC.api.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import ABC.api.model.Follower;


public class FollowUserDao {
	
	public boolean isAlreadyFollowing(Follower obj){
		
		boolean isFollowing=true;
		try{
			Session session = SessionUtil.getSession();
			String hql="From Follower f where f.id1= :id1 AND f.id2 = :id2";
			Query query = session.createQuery(hql);
			query.setParameter("id1", obj.getId1());
			query.setParameter("id2", obj.getId2());
			
			List<Follower> follows = query.list();
			session.close();
			
			if(follows.isEmpty()){
				isFollowing=false;
			}	
			else{
				isFollowing=true;
			}
			}
			catch(Exception e){
				System.out.println("Error at Follower Dao."+e.getMessage());
			}
		return isFollowing;	
		
	}
	
	public String doFollowUserDao(Follower obj){
		
		String status="fail";
		int rowsUpdated=0;
		try{
				
		if(!isAlreadyFollowing(obj)){
			
		Session session = SessionUtil.getSession();
		Transaction tx = session.beginTransaction();
		
		rowsUpdated= (int) session.save(obj);
		
		tx.commit();
		session.close();
		}
		else{
			System.out.println("Already Following User"+obj.getId2());
		}
		
		if(rowsUpdated>0){
			status="success";
		}
		else{
			throw new Exception("Error while following user");
		}
		}
		catch(Exception e){
			System.out.println("Error at Follower Dao."+e.getMessage());
		}
		
		return status;
	}	
	
}
