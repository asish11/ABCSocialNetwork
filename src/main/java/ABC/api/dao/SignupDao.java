package ABC.api.dao;


import org.hibernate.Session;
import org.hibernate.Transaction;

import ABC.api.model.Signup;
import ABC.api.model.User;


public class SignupDao {

	public String doSignupDao(Signup obj){
		
		String status=null;
		User user = new User(obj.getName(),obj.getCompanyName(),obj.getEmail(),obj.getPassword());
		Integer user_id=null;
		try {
						
			Session session = SessionUtil.getSession();        
	        Transaction tx = session.beginTransaction();
	        user_id=(Integer) session.save(user);
	        
	        if(user_id.intValue()>0){
	        	status="success";
				System.out.println("Insert Success. User ID created :"+user_id.intValue());

	        }
	        
	        tx.commit();
	        session.close();
			
		}
		catch(Exception e) {
			System.out.println("Insert Failed. More Info :"+e.getMessage());
			status="fail";
		}
		
		
		
		return status;
		
	}
	
	
	
}
