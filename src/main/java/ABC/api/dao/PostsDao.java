package ABC.api.dao;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import ABC.api.model.Posts;

public class PostsDao {
	
	 public List<Posts> getFeedDao(int id){
	
		 List<Posts> records = new ArrayList();
		 
		 try{
			 
			 //convert to GMT
			 SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			 sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
			 
			 //Fetch last 24hrs date
			 Calendar cal = Calendar.getInstance();
			 cal.add(Calendar.DATE, -2);
			 String datestring=sdf.format(cal.getTime());
			 
			 //System.out.println("Date 0 = "+ datestring);
			 
			 //form final date object with last 24hrs time stamp
			 Date date = sdf.parse(datestring);
			 
			 //System.out.println("Date = "+ date.toString());
			 
			 Session session= SessionUtil.getSession();
			 String hql="From Posts p where createdDate >= :date AND p.userId IN ("+"select id2 from Follower f where f.id1= :id "+")";
			 Query query = session.createQuery(hql);
			 query.setParameter("id", id);
			 query.setParameter("date",date);
			 records=query.list();
			 session.close();
			 
			 
		 }
		 catch(Exception e){
			 
			 System.out.println("Error at PostsDao."+e.getMessage());
			 
		 }
		 
		return records;
	 }
	
	 public String writePostDao(Posts obj){
		 
		 String status="fail";
		 int rows=0;
		 
		 try{
			 
			 Session session=SessionUtil.getSession();
			 Transaction tx = session.beginTransaction();
			 rows=(int) session.save(obj);
			 if(rows>0){
				 status="success";
			 }
			 else{
				 throw new Exception("Could not write post");
			 }
			 
			 tx.commit();
			 session.close();
			 
		 }
		 catch(Exception e){
			 
			 System.out.println("Error at PostsDao. "+e.getMessage());
		 }
		 
		 return status;
	 }

}
