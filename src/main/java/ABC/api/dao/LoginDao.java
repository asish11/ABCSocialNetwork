package ABC.api.dao;

import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.Session;

import ABC.api.model.Login;
import ABC.api.model.User;

public class LoginDao {
		
	public User doLoginDao(Login obj) {
		
		User loginResp = null;
		
		try{			
		
		Session session = SessionUtil.getSession();
		String hql="From User u where u.email= :email AND u.password = :password";
		Query query = session.createQuery(hql);
		query.setParameter("email", obj.getEmail());
		query.setParameter("password", obj.getPassword());
		List<User> users = query.list();
		session.close();
		if(!users.isEmpty()){
			loginResp=users.get(0);
		}
		else{
			throw new Exception("Invalid Credentials");
		}
		
		}
		catch(Exception e){
			System.out.println("Error at Login Dao :"+e.getMessage());
		}
		
		return loginResp;
		
		
		
		
	}

}
